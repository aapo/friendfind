This is server-side of Friendfind.

As default it listen port 9002.
It serves files from directory "../client" ('www-root')

#Dependencies
npm install socket.io

#Starting
node friendfind-server.js
